#!/bin/bash -v

# wait for db to be ready
sleep 5

# stop on errors
set -e

echo "-----------------------------------------------------------------------"
echo "running LinuxFr docker entrypoint"
echo "-----------------------------------------------------------------------"

cd /app/linuxfr.org

# link : https://github.com/linuxfrorg/linuxfr.org/blob/master/config/secrets.yml.sample
# generate random secret
export SECRET_PUSH=$(openssl rand -hex 30)
export SECRET_DEVISE_PEPPER=$(openssl rand -hex 30)
export SECRET_KEY_BASE=$(openssl rand -hex 30)

# may delete your DB in production environment
# export DISABLE_DATABASE_ENVIRONMENT_CHECK=1

# run setup only once time
# don't run setup if linuxfr db exists
[ ! -f /var/tmp/setup ] && (
  echo "show databases;" | mysql -h db -u root -proot  | grep linuxfr || ./bin/rails db:setup ; touch /var/tmp/setup
)

# run upgrade as needed.
echo "-----------------------------------------------------------------------"
echo " Linuxfr : uograde"
echo "-----------------------------------------------------------------------"
./bin/update

echo "-----------------------------------------------------------------------"
echo " Linuxfr : running server"
echo "-----------------------------------------------------------------------"
./bin/rails server -b 0.0.0.0

